## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository -- used for GPM NestJS testing TEM -- slides at https://slides.com/joemeilinger/unit-testing-in-nestjs#/

## Installation

```bash
$ npx yarn
```

## Running the app

```bash
# development
$ npx yarn run start

# watch mode
$ npx yarn run start:dev

# production mode
$ npx yarn run start:prod
```

## Test

```bash
# unit tests
$ npx yarn run test

# e2e tests
$ npx yarn run test:e2e

# test coverage
$ npx yarn run test:cov
```
