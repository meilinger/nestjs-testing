import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { DBService } from './app.service';

class DBServiceMock {
  getValue(): string {
    return 'Do some really expensive DB operation and return the value'
  }
}

describe('AppController', () => {
  let appController: AppController;

  beforeAll(async () => {
    const DBServiceProvider = {
      provide: DBService,
      // We can inject a mock of the DBService using the "useClass" to specify the mock serice vs the actual service and let the
      // dependency injection of NestJS handle things for us.
      useClass: DBService
    }

    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [DBServiceProvider],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Do some really expensive DB operation and return the value"', () => {
      expect(appController.getDatabaseValue()).toBe('Do some really expensive DB operation and return the value');
    });
  });
});
