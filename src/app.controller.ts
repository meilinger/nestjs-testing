import { Controller, Get } from '@nestjs/common';
import { DBService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly dbService: DBService) {}

  @Get()
  getDatabaseValue(): string {
    return this.dbService.getValue();
  }
}
