import { Injectable } from '@nestjs/common';

@Injectable()
export class DBService {
  getValue(): string {
    // Simulate DB delay
    for (let i = 0; i < 10000000000; i++) {
      let a = 2 ** 2;
    }
    return 'Do some really expensive DB operation and return the value';
  }
}
